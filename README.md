## Code Quality

| Statements                                                                               | Branches                                                                             | Functions                                                                              | Lines                                                                          |
| ---------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------ |
| ![Statements](https://img.shields.io/badge/statements-100%25-brightgreen.svg?style=flat) | ![Branches](https://img.shields.io/badge/branches-100%25-brightgreen.svg?style=flat) | ![Functions](https://img.shields.io/badge/functions-100%25-brightgreen.svg?style=flat) | ![Lines](https://img.shields.io/badge/lines-100%25-brightgreen.svg?style=flat) |

# MyNews TEST MOBILE ENGINEER

i have finished for test mobile engineer that i should create news app using NewsAPI. At below i will attach several proof image for this test.

## Image proof for News List
![Screenshot_1692942820](https://github.com/whayu901/MyRukit/assets/32776398/3fcfbf79-3300-4941-870e-c428e3806832)


## Image proof for Detail News List
![Screenshot_1692942842](https://github.com/whayu901/MyRukit/assets/32776398/d546e142-3d96-4d4a-89b2-e1b38823bd8c)

## Image proof for Saved News
![Screenshot_1692942849](https://github.com/whayu901/MyRukit/assets/32776398/be5c7604-f1c5-4698-a999-241e3d8e4c0e)

## Image proof for Search News
![Screenshot_1692942835](https://github.com/whayu901/MyRukit/assets/32776398/76dc8795-c8e2-4449-b400-b926285830c6)

