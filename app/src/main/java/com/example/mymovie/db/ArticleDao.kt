package com.example.mymovie.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mymovie.models.Article

@Dao

interface ArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(article: Article): Long // function(parameter):return --> here we return ID

    @Query("SELECT* FROM articles")
    fun getAllArticles(): LiveData<List<Article>>

    @Delete
    suspend fun deleteArticle(article: Article)
}