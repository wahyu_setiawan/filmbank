package com.example.mymovie.api

import com.example.mymovie.models.NewsResponse
import com.example.mymovie.utils.Constants.Companion.API_KEY
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    @GET("v2/top-headlines")

    suspend fun getBreakingNews(
        //request parameters to function
        @Query("country")
        countryCode: String = "id", //default to us

        @Query("page")  //to paginate the request
        pageNumber: Int= 1,

        @Query("apiKey")
        apiKey: String= API_KEY

    ): Response<NewsResponse> //return response


    @GET("v2/everything")

    //function
    //async
    //coroutine
    suspend fun searchForNews(
        //request parameters to function
        @Query("q")
        searchQuery: String,
        @Query("page")  //to paginate the request
        pageNumber: Int= 1,
        @Query("apiKey")
        apiKey: String= API_KEY
    ): Response<NewsResponse> //return response
}