package com.example.mymovie.utils

class Constants {
    companion object{
        const val API_KEY= "8a42bbce38c54e19969a80174b1be5af"
        const val BASE_URL= "https://newsapi.org"
        const val SEARCH_DELAY= 500L
        const val QUERY_PAGE_SIZE= 20
    }
}